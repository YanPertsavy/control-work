#include <iostream>
#include <fstream>
#include <string>

using namespace std;


int calculateSum(string);
int calculateSumFromFile(string);
void calculateSumTest(string);
/*
int binaryToDecimal(string);
*/

int main()
{
	string fileName = "test.txt";
	ifstream in(fileName,ios::binary);
	calculateSumTest("test.txt");

}

int calculateSum(string source)
{
	int sum = 0;
	string binstr;
	for (char c : source) {
		if (isdigit(c)) {
			binstr += c;
		}
		else if (!binstr.empty())
		{
			sum += stoi(binstr, nullptr,2);
			binstr.clear();
		}
	}
	if (!binstr.empty()) {
		sum += stoi(binstr, nullptr,2);
	}
	return sum;
}

int calculateSumFromFile(string fileName)
{
	int sum = 0;
	string line;
	while (getline(fileName, line))//не понимаю почему не работает
	{
		sum += calculateSum(line);
	}
	return sum;
}
//
//int binaryToDecimal(string binary)
//{
//	//TODO
//}
//
void calculateSumTest(string fileName)
{
	bool result = calculateSum("1+-0100+*** 1000") == 13;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1000001+-1000+* 100** 1--- 0000001") == 79;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("10000000000011111+11+*** 1111000111") == 66537;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1111111111111000011111+-11111111111+*   1111** -22--- ") == 4195885;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSumFromFile(fileName) == 4262515;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
}