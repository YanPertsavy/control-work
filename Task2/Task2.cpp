#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct Map
{
	char symbol;
	int number;
	static const int count = 26;
};

Map* createMap();
void writeMapToFile(ofstream& fileMap, Map*);
void displayContentFileWithMap(ofstream& fileMap);
//void encoding(string, string, Map*);
//void encoding(string, string, Map*);
//void decoding(string, string, Map*);
//Map* createDectionary(string);
//int encoding(char, Map*);
//char decoding(int, Map*);

int main()
{


	string fileMap = "codes";
	ofstream out(fileMap,ios::binary);
	Map* map = createMap();
	writeMapToFile(out, map);
	ifstream in(fileMap);
	displayContentFileWithMap(out);
	//encoding("input.txt", "result.txt", map);
	//decoding("result.txt", "output.txt", map);
}

Map* createMap()
{
	Map* map = new Map;

	char currentSymbol = 'A';
	int currentNumber = 26;

	for (int i = 0; i < map->count; i++)
	{
		map[i].symbol = currentSymbol;
		map[i].number = currentNumber;

		currentSymbol++;
		currentNumber--;
	}

	return map;
}

void writeMapToFile(ofstream& fileMap, Map* map)
{
	if (!fileMap.is_open())
	{
		throw runtime_error("Error");
	}
	for (int i = 0; i < map->count; i++)
	{
		fileMap << map[i].symbol<<","<<map[i].number;
	}
	fileMap.close();
}

void displayContentFileWithMap(ifstream& in)
{
	if (!in.is_open())
	{
		throw runtime_error("Error");
	}

	string line;
	while (getline(in, line))
	{
		cout << line << endl;
	}

	in.close();
}

//Map* createDectionary(string fileName)
//{
//	//TODO
//}

//int encoding(char symbol, Map* map)
//{
//	//TODO
//}

//void encoding(string input, string output, Map* map)
//{
//	//TODO
//}

//void decoding(string input, string output, Map const* map)
//{
//	//TODO
//}

//char decoding(int item, Map const* map)
//{
//	//TODO
//}